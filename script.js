


//add the following users:

db.users.insertMany([
	{"firstName": "Diane", "lastName": "Murphy", "email": "dmurphy@mail.com", "isAdmin": false, "isActive": true},
	{"firstName": "Mary", "lastName": "Patterson", "email": "mpatterson@mail.com", "isAdmin": false, "isActive": true},
	{"firstName": "Jeff", "lastName": "Firrelli", "email": "jfirrelli@mail.com", "isAdmin": false, "isActive": true},
	{"firstName": "Gerard", "lastName": "Bondur", "email": "gbondur@mail.com", "isAdmin": false, "isActive": true},
	{"firstName": "Pamela", "lastName": "Castillo", "email": "pcastillo@mail.com", "isAdmin": true, "isActive": false},
	{"firstName": "George", "lastName": "Vanauf", "email": "gvanauf@mail.com", "isAdmin": true, "isActive": true}
]);

//add the following courses:

db.courses.insertMany([
	{"name": "Professional Development", "price": 10000.00},
	{"name": "Business Processing", "price": 13000.00}
]);

//get user IDs and add them as enrollees of the courses(update)

db.courses.updateOne(
	{"name": "Professional Development"},
   	{ $set: { "enrollees": [ObjectId("6125b4759d98952d8dc4e0ab"), ObjectId("6125b4759d98952d8dc4e0ad")]} }
);

db.courses.updateOne(
	{"name": "Business Processing"},
   	{ $set: { "enrollees": [ObjectId("6125b4759d98952d8dc4e0ae"), ObjectId("6125b4759d98952d8dc4e0ac")]} }
);

//get the users who are not administrators

db.users.find({"isAdmin": "false"});